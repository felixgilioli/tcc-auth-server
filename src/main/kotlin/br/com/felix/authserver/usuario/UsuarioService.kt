package br.com.felix.authserver.usuario

import br.com.felix.authserver.core.crud.CrudService

interface UsuarioService : CrudService<Usuario, Long> {
}