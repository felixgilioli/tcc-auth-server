package br.com.felix.authserver.usuario

import org.springframework.data.jpa.repository.JpaRepository

interface UsuarioRepository : JpaRepository<Usuario, Long> {

    fun findByLogin(username: String): Usuario?
}