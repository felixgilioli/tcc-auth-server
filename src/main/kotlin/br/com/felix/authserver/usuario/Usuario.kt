package br.com.felix.authserver.usuario

import br.com.felix.authserver.authority.Authority
import org.hibernate.validator.constraints.Length
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*

@Entity
data class Usuario(

        @Length(min = 2, max = 100, message = "nome deve estar entre 2 e 100 caracteres.")
        @Column(nullable = false, length = 100)
        val nome: String = "",

        @Length(min = 2, max = 45, message = "login deve estar entre 2 e 45 caracteres.")
        @Column(nullable = false, length = 45)
        val login: String = "",

        @Column(nullable = false)
        val senha: String = "",

        @ElementCollection
        val authorities: MutableSet<Authority> = mutableSetOf(),

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val usuarioId: Long? = null

) : UserDetails {

        override fun getAuthorities(): MutableCollection<out GrantedAuthority> = authorities

        override fun isEnabled() = true

        override fun getUsername() = login

        override fun isCredentialsNonExpired() = true

        override fun getPassword() = senha

        override fun isAccountNonExpired() = true

        override fun isAccountNonLocked() = true
}
