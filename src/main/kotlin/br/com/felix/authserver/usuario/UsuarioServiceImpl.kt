package br.com.felix.authserver.usuario

import br.com.felix.authserver.core.crud.CrudServiceImpl
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UsuarioServiceImpl(private val repository: UsuarioRepository) : UsuarioService, CrudServiceImpl<Usuario, Long>(), UserDetailsService {

    override fun getRepository() = repository

    @Transactional(readOnly = true)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = repository.findByLogin(username)
        user?.authorities?.size

        return user ?: throw UsernameNotFoundException(username)
    }

}

//$2a$10$76pniHAgi//jF/ofjc6vneElrG95Ij.Qwt/OX4k9fU.ImTtT7eUbq