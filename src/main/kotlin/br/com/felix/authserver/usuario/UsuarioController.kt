package br.com.felix.authserver.usuario

import br.com.felix.authserver.core.crud.CrudController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("usuario")
class UsuarioController(private val service: UsuarioService) : CrudController<Usuario, Long>() {

    override fun getService() = service

    @GetMapping("user-info")
    fun user(principal: Principal): Principal {
        return principal
    }
}