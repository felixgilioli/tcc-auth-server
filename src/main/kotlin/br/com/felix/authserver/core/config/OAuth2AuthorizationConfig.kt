package br.com.felix.authserver.core.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore
import javax.sql.DataSource


@Configuration
@EnableAuthorizationServer
class OAuth2AuthorizationConfig(@Qualifier("authenticationManagerBean") private val authenticationManager: AuthenticationManager,
                                @Qualifier("usuarioServiceImpl") private val userDetailsService: UserDetailsService,
                                private val dataSource: DataSource) : AuthorizationServerConfigurerAdapter() {

    @Bean
    fun loggingExceptionTranslator(): WebResponseExceptionTranslator<OAuth2Exception> {
        return object : DefaultWebResponseExceptionTranslator() {
            @Throws(Exception::class)
            override fun translate(e: Exception): ResponseEntity<OAuth2Exception> {
                e.printStackTrace()
                val responseEntity = super.translate(e)
                val headers = HttpHeaders()
                headers.setAll(responseEntity.headers.toSingleValueMap())
                val excBody = responseEntity.body
                return ResponseEntity(excBody, headers, responseEntity.statusCode)
            }
        }
    }

    @Bean
    @Primary
    fun tokenStore(): TokenStore {
        return JdbcTokenStore(dataSource)
    }

    @Bean
    @Primary
    fun authorizationServerTokenServices(): AuthorizationServerTokenServices {
        return CustomTokenService(tokenStore())
    }

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.inMemory()
                .withClient("felix")
                // Basic ZmVsaXg6ZGY2NTRnMWE2NXIxZzZhd2VyZ3JhOGcxYTZyZTFnNmFldzFyZzY=
                .secret(BCryptPasswordEncoder().encode("df654g1a65r1g6awergra8g1a6re1g6aew1rg6"))
                .autoApprove(true)
                .authorizedGrantTypes("authorization_code", "refresh_token", "password", "client_credentials")
                .scopes("openid")
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        endpoints
                .exceptionTranslator(loggingExceptionTranslator())
                .tokenStore(tokenStore())
                .tokenServices(authorizationServerTokenServices())
                .userDetailsService(userDetailsService)
                .authenticationManager(authenticationManager)
    }

    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")
                .passwordEncoder(BCryptPasswordEncoder())

    }
}