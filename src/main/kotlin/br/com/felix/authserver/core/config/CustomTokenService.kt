package br.com.felix.authserver.core.config

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.oauth2.common.*
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException
import org.springframework.security.oauth2.common.exceptions.InvalidScopeException
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException
import org.springframework.security.oauth2.provider.*
import org.springframework.security.oauth2.provider.token.*
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken
import org.springframework.transaction.annotation.Transactional
import java.util.*


open class CustomTokenService(tokenStore: TokenStore) : AuthorizationServerTokenServices, ResourceServerTokenServices, ConsumerTokenServices {


    private var refreshTokenValiditySeconds = 60 * 60 * 24 * 30 // padrao 30 days

    private var accessTokenValiditySeconds = 60 * 60 * 12 // padrao 12 horas

    private var supportRefreshToken = false

    private var reuseRefreshToken = true

    private var tokenStore: TokenStore? = tokenStore

    private var clientDetailsService: ClientDetailsService? = null

    private var accessTokenEnhancer: TokenEnhancer? = null

    private var authenticationManager: AuthenticationManager? = null

    @Transactional
    @Throws(AuthenticationException::class)
    override fun createAccessToken(authentication: OAuth2Authentication): OAuth2AccessToken {

        val existingAccessToken = tokenStore!!.getAccessToken(authentication)
        var refreshToken: OAuth2RefreshToken? = null
        if (existingAccessToken != null) {
            if (existingAccessToken.isExpired) {
                if (existingAccessToken.refreshToken != null) {
                    refreshToken = existingAccessToken.refreshToken
                    tokenStore!!.removeRefreshToken(refreshToken)
                }
                tokenStore!!.removeAccessToken(existingAccessToken)
            } else {
                tokenStore!!.storeAccessToken(existingAccessToken, authentication)
                return existingAccessToken
            }
        }

        if (refreshToken == null) {
            refreshToken = createRefreshToken(authentication)
        } else if (refreshToken is ExpiringOAuth2RefreshToken) {
            val expiring = refreshToken as ExpiringOAuth2RefreshToken?
            if (System.currentTimeMillis() > expiring!!.expiration.time) {
                refreshToken = createRefreshToken(authentication)
            }
        }

        val accessToken = createAccessToken(authentication, refreshToken)
        tokenStore!!.storeAccessToken(accessToken, authentication)
        refreshToken = accessToken.refreshToken
        if (refreshToken != null) {
            tokenStore!!.storeRefreshToken(refreshToken, authentication)
        }
        return accessToken

    }

    @Transactional(noRollbackFor = [InvalidTokenException::class, InvalidGrantException::class])
    @Throws(AuthenticationException::class)
    override fun refreshAccessToken(refreshTokenValue: String?, tokenRequest: TokenRequest): OAuth2AccessToken {

        if (!supportRefreshToken) {
            throw InvalidGrantException("Invalid refresh token: $refreshTokenValue")
        }

        var refreshToken: OAuth2RefreshToken? = tokenStore!!.readRefreshToken(refreshTokenValue)
                ?: throw InvalidGrantException("Invalid refresh token: $refreshTokenValue")

        var authentication = tokenStore!!.readAuthenticationForRefreshToken(refreshToken)
        if (this.authenticationManager != null && !authentication.isClientOnly) {
            var user: Authentication = PreAuthenticatedAuthenticationToken(authentication.userAuthentication, "", authentication.authorities)
            user = authenticationManager!!.authenticate(user)
            val details = authentication.details
            authentication = OAuth2Authentication(authentication.oAuth2Request, user)
            authentication.details = details
        }
        val clientId = authentication.oAuth2Request.clientId
        if (clientId == null || clientId != tokenRequest.clientId) {
            throw InvalidGrantException("Wrong client for this refresh token: $refreshTokenValue")
        }

        tokenStore!!.removeAccessTokenUsingRefreshToken(refreshToken)

        if (isExpired(refreshToken!!)) {
            tokenStore!!.removeRefreshToken(refreshToken)
            throw InvalidTokenException("Invalid refresh token (expired): " + refreshToken!!)
        }

        authentication = createRefreshedAuthentication(authentication, tokenRequest)

        if (!reuseRefreshToken) {
            tokenStore!!.removeRefreshToken(refreshToken)
            refreshToken = createRefreshToken(authentication)
        }

        val accessToken = createAccessToken(authentication, refreshToken!!)
        tokenStore!!.storeAccessToken(accessToken, authentication)
        if (!reuseRefreshToken) {
            tokenStore!!.storeRefreshToken(accessToken.refreshToken, authentication)
        }
        return accessToken
    }

    override fun getAccessToken(authentication: OAuth2Authentication): OAuth2AccessToken {
        return tokenStore!!.getAccessToken(authentication)
    }

    private fun createRefreshedAuthentication(authentication: OAuth2Authentication, request: TokenRequest): OAuth2Authentication {
        var narrowed = authentication
        val scope = request.scope
        var clientAuth = authentication.oAuth2Request.refresh(request)
        if (scope != null && !scope.isEmpty()) {
            val originalScope = clientAuth.scope
            if (originalScope == null || !originalScope.containsAll(scope)) {
                throw InvalidScopeException("Unable to narrow the scope of the client authentication to " + scope
                        + ".", originalScope)
            } else {
                clientAuth = clientAuth.narrowScope(scope)
            }
        }
        narrowed = OAuth2Authentication(clientAuth, authentication.userAuthentication)
        return narrowed
    }

    protected fun isExpired(refreshToken: OAuth2RefreshToken): Boolean {
        return if (refreshToken is ExpiringOAuth2RefreshToken) {
            refreshToken.expiration == null || System.currentTimeMillis() > refreshToken.expiration.time
        } else false
    }

    override fun readAccessToken(accessToken: String): OAuth2AccessToken {
        return tokenStore!!.readAccessToken(accessToken)
    }

    @Throws(AuthenticationException::class, InvalidTokenException::class)
    override fun loadAuthentication(accessTokenValue: String): OAuth2Authentication {
        val accessToken = tokenStore!!.readAccessToken(accessTokenValue)
        if (accessToken == null) {
            throw InvalidTokenException("Invalid access token: $accessTokenValue")
        } else if (accessToken.isExpired) {
            tokenStore!!.removeAccessToken(accessToken)
            throw InvalidTokenException("Access token expired: $accessTokenValue")
        }

        val result = tokenStore!!.readAuthentication(accessToken)
                ?: // in case of race condition
                throw InvalidTokenException("Invalid access token: $accessTokenValue")
        if (clientDetailsService != null) {
            val clientId = result.oAuth2Request.clientId
            try {
                clientDetailsService!!.loadClientByClientId(clientId)
            } catch (e: ClientRegistrationException) {
                throw InvalidTokenException("Client not valid: $clientId", e)
            }

        }
        return result
    }

    fun getClientId(tokenValue: String): String {
        val authentication = tokenStore!!.readAuthentication(tokenValue)
                ?: throw InvalidTokenException("Invalid access token: $tokenValue")
        val clientAuth = authentication.oAuth2Request
                ?: throw InvalidTokenException("Invalid access token (no client id): $tokenValue")
        return clientAuth.clientId
    }

    override fun revokeToken(tokenValue: String): Boolean {
        val accessToken = tokenStore!!.readAccessToken(tokenValue) ?: return false
        if (accessToken.refreshToken != null) {
            tokenStore!!.removeRefreshToken(accessToken.refreshToken)
        }
        tokenStore!!.removeAccessToken(accessToken)
        return true
    }

    private fun createRefreshToken(authentication: OAuth2Authentication): OAuth2RefreshToken? {
        if (!isSupportRefreshToken(authentication.oAuth2Request)) {
            return null
        }
        val validitySeconds = getRefreshTokenValiditySeconds(authentication.oAuth2Request)
        val value = UUID.randomUUID().toString()
        return if (validitySeconds > 0) {
            DefaultExpiringOAuth2RefreshToken(value, Date(System.currentTimeMillis() + validitySeconds * 1000L))
        } else DefaultOAuth2RefreshToken(value)
    }

    private fun createAccessToken(authentication: OAuth2Authentication, refreshToken: OAuth2RefreshToken?): OAuth2AccessToken {
        val token = DefaultOAuth2AccessToken(UUID.randomUUID().toString())
        val validitySeconds = getAccessTokenValiditySeconds(authentication.oAuth2Request)
        if (validitySeconds > 0) {
            token.expiration = Date(System.currentTimeMillis() + validitySeconds * 1000L)
        }
        token.refreshToken = refreshToken
        token.scope = authentication.oAuth2Request.scope

        return if (accessTokenEnhancer != null) accessTokenEnhancer!!.enhance(token, authentication) else token
    }

    protected fun getAccessTokenValiditySeconds(clientAuth: OAuth2Request): Int {
        if (clientDetailsService != null) {
            val client = clientDetailsService!!.loadClientByClientId(clientAuth.clientId)
            val validity = client.accessTokenValiditySeconds
            if (validity != null) {
                return validity
            }
        }
        return accessTokenValiditySeconds
    }

    protected fun getRefreshTokenValiditySeconds(clientAuth: OAuth2Request): Int {
        if (clientDetailsService != null) {
            val client = clientDetailsService!!.loadClientByClientId(clientAuth.clientId)
            val validity = client.refreshTokenValiditySeconds
            if (validity != null) {
                return validity
            }
        }
        return refreshTokenValiditySeconds
    }

    protected fun isSupportRefreshToken(clientAuth: OAuth2Request): Boolean {
        if (clientDetailsService != null) {
            val client = clientDetailsService!!.loadClientByClientId(clientAuth.clientId)
            return client.authorizedGrantTypes.contains("refresh_token")
        }
        return this.supportRefreshToken
    }

    fun setTokenEnhancer(accessTokenEnhancer: TokenEnhancer) {
        this.accessTokenEnhancer = accessTokenEnhancer
    }

    fun setRefreshTokenValiditySeconds(refreshTokenValiditySeconds: Int) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds
    }

    fun setAccessTokenValiditySeconds(accessTokenValiditySeconds: Int) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds
    }

    fun setSupportRefreshToken(supportRefreshToken: Boolean) {
        this.supportRefreshToken = supportRefreshToken
    }

    fun setReuseRefreshToken(reuseRefreshToken: Boolean) {
        this.reuseRefreshToken = reuseRefreshToken
    }

    fun setTokenStore(tokenStore: TokenStore) {
        this.tokenStore = tokenStore
    }

    fun setAuthenticationManager(authenticationManager: AuthenticationManager) {
        this.authenticationManager = authenticationManager
    }

    fun setClientDetailsService(clientDetailsService: ClientDetailsService) {
        this.clientDetailsService = clientDetailsService
    }

}