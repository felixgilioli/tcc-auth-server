package br.com.felix.authserver.authority

import br.com.felix.authserver.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class AuthorityServiceImpl(private val repository: AuthorityRepository) : AuthorityService, CrudServiceImpl<Authority, Long>() {

    override fun getRepository() = repository
}