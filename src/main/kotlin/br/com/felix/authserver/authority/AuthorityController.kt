package br.com.felix.authserver.authority

import br.com.felix.authserver.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("authority")
class AuthorityController(private val service: AuthorityService) : CrudController<Authority, Long>() {

    override fun getService() = service
}