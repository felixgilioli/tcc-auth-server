package br.com.felix.authserver.authority

import org.springframework.security.core.GrantedAuthority
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
data class Authority(

        @NotBlank(message = "name nao pode ser nulo.")
        @Column(nullable = false)
        val name: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val authorityId: Long? = null
) : GrantedAuthority {

    override fun getAuthority() = name
}