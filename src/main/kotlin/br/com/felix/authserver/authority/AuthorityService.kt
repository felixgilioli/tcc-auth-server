package br.com.felix.authserver.authority

import br.com.felix.authserver.core.crud.CrudService

interface AuthorityService : CrudService<Authority, Long> {
}