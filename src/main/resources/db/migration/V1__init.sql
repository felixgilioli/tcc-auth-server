create table oauth_access_token (
  token_id VARCHAR(255),
  token LONG VARBINARY,
  authentication_id VARCHAR(255) PRIMARY KEY,
  user_name VARCHAR(255),
  client_id VARCHAR(255),
  authentication LONG VARBINARY,
  refresh_token VARCHAR(255)
);

create table oauth_refresh_token (
  token_id VARCHAR(255),
  token LONG VARBINARY,
  authentication LONG VARBINARY
);

CREATE TABLE authority (
  authority_id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (authority_id)
);

CREATE TABLE usuario (
  usuario_id bigint(20) NOT NULL AUTO_INCREMENT,
  login varchar(45) NOT NULL,
  nome varchar(100) NOT NULL,
  senha varchar(255) NOT NULL,
  PRIMARY KEY (usuario_id)
);

insert into usuario(login, nome, senha) values ('felix', 'felix', '$2a$10$76pniHAgi//jF/ofjc6vneElrG95Ij.Qwt/OX4k9fU.ImTtT7eUbq');
